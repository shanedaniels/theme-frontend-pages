<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Coming Soon! :: &nbsp;@setting('platform.app.title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" href="{{ Asset::getUrl('pages/ico/60.png') }}">
    <link rel="apple-touch-icon" sizes=""76x76" href="{{ Asset::getUrl('pages/ico/76.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ Asset::getUrl('pages/ico/120.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ Asset::getUrl('pages/ico/152.png') }}">
    <link rel="icon" type="image/x-icon" href="{{ Asset::getUrl('favicon.ico') }}" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- BEGIN PLUGINS -->
    {{ Asset::queue('pace-theme-flash', 'plugins/pace/pace-theme-flash.css') }}
    {{ Asset::queue('bootstrap', 'plugins/bootstrap/css/bootstrap.min.css') }}
    {{ Asset::queue('font-awesome', 'plugins/font-awesome/css/font-awesome.css') }}
    {{ Asset::queue('swiper', 'plugins/swiper/css/swiper.css') }}
    {{ Asset::queue('pages', 'pages/css/pages.css') }}
    {{ Asset::queue('pages-icons', 'pages/css/pages-icons.css') }}

    @foreach(Asset::getCompiledStyles() as $style)
        <link rel="stylesheet" href="{{ $style }}"/>
    @endforeach
</head>

<body class="pace-white">

    <!-- BEGIN JUMBOTRON -->
    <section class="jumbotron demo-custom-height xs-full-height bg-black" data-pages-bg-image="{{ Asset::getUrl('images/hero_4.jpg') }}">
        <div class="container-xs-height full-height">
            <div class="col-xs-height col-middle text-left">
                <div class="container">

                    <div id="non-subscriber" class="col-sm-7">
                        <h1 class="light text-white">The new website is on its way!</h1>
                        <h4 class="text-white">Be first to find out when we launch the new Shane Daniels.</h4>
                        <form id="mc-signup" class="m-t-25 m-b-20" method="post">
                            <div class="form-group form-group-default input-group no-border input-group-attached col-md-12  col-sm-12 col-xs-12">
                                <label>Email Address</label>
                                <input id="mc-email" name="mc-email" type="email" class="form-control" placeholder="johnsmith@abc.com">

                                    <span class="input-group-btn">
                                        <button class="btn btn-black  btn-cons" type="submit">Subscribe!</button>
                                    </span>
                            </div>
                        </form>
                        <p class="text-white fs-12">Don't worry, we hate spam just as much as you do and will not spam your email.</p>
                    </div>

                    <div id="subscriber" class="col-sm-6" style="display: none;">
                        <h1 class="light text-white">Just one more step...</h1>
                        <h4 class="text-white">Check your email for confirmation.</h4>

                        <p class="text-white fs-12">
                            Once you've confirmed your email address, you'll be first to find out when we Launch the New Shane Daniels.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END JUMBOTRON -->

    <!-- START FOOTER -->
    <section class="p-b-30 p-t-40">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <img src="{{ Asset::getUrl('images/logo_black.png') }}" width="152" height="21" data-src-retina="{{ Asset::getUrl('images/logo_black_2x.png') }}" class="logo inline m-r-50" alt="">
                </div>
                <div class="col-sm-6 text-right font-arial sm-text-left">
                    <p class="fs-11 muted">Copyright &copy; {{ date('Y') }} Shane Daniels. All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </section>
    <!-- END FOOTER -->

    <!-- BEGIN VENDOR JS -->
    {{ Asset::queue('jquery', 'plugins/jquery/jquery-1.11.1.min.js') }}
    {{ Asset::queue('bootstrap', 'plugins/bootstrap/js/bootstrap.js', ['jquery']) }}
    {{ Asset::queue('swiper', 'plugins/swiper/js/swiper.jquery.min.js') }}
    {{ Asset::queue('velocity', 'plugins/velocity/velocity.min.js') }}
    {{ Asset::queue('velocity.ui', 'plugins/velocity/velocity.ui.js') }}
    {{ Asset::queue('jquery.unveil', 'plugins/jquery-unveil/jquery.unveil.min.js') }}
    {{ Asset::queue('pages.frontend', 'pages/js/pages.frontend.js') }}
    {{ Asset::queue('ajaxchimp', 'js/ajaxchimp.js', ['jquery']) }}
    {{ Asset::queue('custom', 'js/custom.js') }}

    @foreach(Asset::getCompiledScripts() as $script)
        <script type="text/javascript" src="{!! $script !!}"></script>
    @endforeach

    <script>

        function mailchimpCallback (resp) {
            if (resp.result === 'success') {
                $("#non-subscriber").hide();
                $("#subscriber").show();
            } else {
                $("#chimp-error").html(resp.msg);
                $("#chimpError").modal('show');
            }
        }

        $(function() {
            $('#mc-signup').ajaxChimp({
                url: '//shanedaniels.us11.list-manage.com/subscribe/post?u=1728a8faef6de97288ca55472&id=a0df97c1e9',
                callback: mailchimpCallback
            });
        });
    </script>

    <!-- Modal -->
    <div class="modal fade slide-up disable-scroll" id="chimpError" tabindex="-1" role="dialog" aria-labelledby="modalSlideUpLabel" aria-hidden="false">
        <div class="modal-dialog ">
            <div class="modal-content-wrapper">
                <div class="modal-content">
                    <div class="modal-header clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            <i class="pg-close fs-14"></i>
                        </button>
                        <h5 class="bold">Whoops...</h5>
                    </div>
                    <div class="modal-body">
                        <p id="chimp-error"></p>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <!-- /.modal-dialog -->
</body>
</html>